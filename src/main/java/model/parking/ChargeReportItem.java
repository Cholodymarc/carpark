package model.parking;

import java.time.LocalDateTime;

public class ChargeReportItem {
    private int vehicleId;
    private int spotId;
    private LocalDateTime entranceTimeStamp;
    private LocalDateTime exitTimeStamp;
    private int price;

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public int getSpotId() {
        return spotId;
    }

    public void setSpotId(int spotId) {
        this.spotId = spotId;
    }

    public LocalDateTime getEntranceTimeStamp() {
        return entranceTimeStamp;
    }

    public void setEntranceTimeStamp(LocalDateTime entranceTimeStamp) {
        this.entranceTimeStamp = entranceTimeStamp;
    }

    public LocalDateTime getExitTimeStamp() {
        return exitTimeStamp;
    }

    public void setExitTimeStamp(LocalDateTime exitTimeStamp) {
        this.exitTimeStamp = exitTimeStamp;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
