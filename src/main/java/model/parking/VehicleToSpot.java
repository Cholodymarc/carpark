package model.parking;

import java.time.LocalDateTime;

public class VehicleToSpot {
    private int id;
    private int spotId;
    private int vehicleId;
    private LocalDateTime entranceTimeStamp;
    private LocalDateTime exitTimeStamp;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSpotId() {
        return spotId;
    }

    public void setSpotId(int spotId) {
        this.spotId = spotId;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public LocalDateTime getEntranceTimeStamp() {
        return entranceTimeStamp;
    }

    public void setEntranceTimeStamp(LocalDateTime entranceTimeStamp) {
        this.entranceTimeStamp = entranceTimeStamp;
    }

    public LocalDateTime getExitTimeStamp() {
        return exitTimeStamp;
    }

    public void setExitTimeStamp(LocalDateTime exitTimeStamp) {
        this.exitTimeStamp = exitTimeStamp;
    }
}
