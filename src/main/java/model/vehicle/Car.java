package model.vehicle;

public abstract class Car extends Vehicle {
    public abstract int getPrice();
}
