package model.vehicle;

public class PassengerCar extends Car {
    private final int price  = 10 ;

    public int getPrice() {
        return price;
    }
}
