package model.vehicle;

public class DeliveryCar extends Car {

    private final int price = 15;

    public int getPrice() {
        return price;
    }
}
