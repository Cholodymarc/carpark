package model.vehicle;

public abstract class Vehicle {
    protected int id;

    public abstract int getPrice();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
