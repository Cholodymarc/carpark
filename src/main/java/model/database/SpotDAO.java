package model.database;

import model.parking.Spot;
import model.parking.VehicleToSpot;

import java.util.List;

public interface SpotDAO {
    List<VehicleToSpot> getVehicleToSpots();
    List<Spot> getSpots();
    Spot getSpotOnId(int spotId);
    List<VehicleToSpot> getArchivedVehicleToSpots();
}
