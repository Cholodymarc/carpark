package model.database;

import model.parking.ChargeReportItem;

import java.util.ArrayList;
import java.util.List;

public class Database {

    private static Database instance;
    SpotDAO spotDAO;
    VehicleDAO vehicleDAO;

    Database() {
    }

    public static Database getInstance() {
        if (instance == null) {
            instance = new Database();
            instance.spotDAO = new DatabaseSpotDAO();
            instance.vehicleDAO = new DatabaseVehicleDAO();
        }
        return instance;
    }

    public List<ChargeReportItem> getDailyChargeReport() {
        List<ChargeReportItem> dailyChargeReport = new ArrayList<>();

        spotDAO.getArchivedVehicleToSpots().stream()
                .forEach(vehicleToSpot -> {
                    ChargeReportItem chargeReportItem = new ChargeReportItem();
                    chargeReportItem.setEntranceTimeStamp(vehicleToSpot.getEntranceTimeStamp());
                    chargeReportItem.setExitTimeStamp(vehicleToSpot.getExitTimeStamp());
                    chargeReportItem.setVehicleId(vehicleToSpot.getVehicleId());
                    chargeReportItem.setSpotId(vehicleToSpot.getSpotId());
                    chargeReportItem.setPrice(vehicleDAO.getVehicleOnId(vehicleToSpot.getVehicleId()).getPrice()); //should be price multiplied by number of days
                    dailyChargeReport.add(chargeReportItem);
                });

        return dailyChargeReport;
    }
}
