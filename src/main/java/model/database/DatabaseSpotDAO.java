package model.database;

import model.parking.Spot;
import model.parking.VehicleToSpot;
import model.vehicle.Vehicle;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class DatabaseSpotDAO implements SpotDAO {
    static final int NUMBER_OF_AVAILABLE_SPOTS = 100;
    static final int SPOT_NOT_FOUND_FLAG = -1;

    private List<Spot> spots = new ArrayList<>();
    private List<VehicleToSpot> vehicleToSpots = new ArrayList<>();
    private List<VehicleToSpot> archivedVehicleToSpots = new ArrayList<>();

    private void createSpotId(Spot spot) {
        if (spots.isEmpty()) {
            spot.setId(1);
        } else {
            spot.setId(spots.get(spots.size() - 1).getId() + 1);
        }
        spots.add(spot);
    }

    private void addSpot(Spot spot) {
        createSpotId(spot);
        spot.setAvailable(true);
        spots.add(spot);
    }

    public void generateAvailableSpots() {
        for (int i = 0; i <= NUMBER_OF_AVAILABLE_SPOTS; i++) {
            addSpot(new Spot());
        }
    }

    public void createVehicleToSpotId(VehicleToSpot vehicleToSpot) {
        if (vehicleToSpots.isEmpty()) {
            vehicleToSpot.setId(1);
        } else {
            vehicleToSpot.setId(vehicleToSpots.get(vehicleToSpots.size() - 1).getId() + 1);
        }
    }

    private void addVehicleToSpot(int vehicleId, int spotId) {
        Spot takenSpot = getSpotOnId(spotId);
        VehicleToSpot vehicleToSpot = new VehicleToSpot();

        createVehicleToSpotId(vehicleToSpot);
        vehicleToSpot.setVehicleId(vehicleId);
        vehicleToSpot.setSpotId(spotId);
        vehicleToSpot.setEntranceTimeStamp(LocalDateTime.now());
        takenSpot.setAvailable(false);
        vehicleToSpots.add(vehicleToSpot);
    }

    public boolean takeSpot(int vehicleId) {
        int spotId = findFirstAvailableSpot();
        if (spotId == SPOT_NOT_FOUND_FLAG) {
            return false;
        }
        addVehicleToSpot(vehicleId, spotId);
        return true;
    }

    public boolean takeSpot(Vehicle vehicle){
        return takeSpot(vehicle.getId());
    }

    public boolean freeUpSpot(int vehicleId) {
        Optional<VehicleToSpot> searchedVehicleToSpot = vehicleToSpots.stream()
                .filter(vehicleToSpot -> vehicleToSpot.getVehicleId() == vehicleId)
                .findFirst();
        if(searchedVehicleToSpot.isPresent()){
            searchedVehicleToSpot.get().setExitTimeStamp(LocalDateTime.now());
            int freeUpSpotId = searchedVehicleToSpot.get().getSpotId();
            getSpotOnId(freeUpSpotId).setAvailable(true);
            archivedVehicleToSpots.add(searchedVehicleToSpot.get());

            vehicleToSpots = vehicleToSpots.stream()
                    .filter(vehicleToSpot -> vehicleToSpot.getId() != searchedVehicleToSpot.get().getId())
                    .collect(Collectors.toList());
            return true;
        } else {
            return false;
        }
    }

    public boolean freeUpSpot(Vehicle vehicle){
        return freeUpSpot(vehicle.getId());
    }

    public int findFirstAvailableSpot() {
        int firstAvailableSpotId;
        Optional<Spot> availableSpot = spots.stream()
                .filter(spot -> spot.isAvailable()).findFirst();
        if(availableSpot.isPresent()){
            firstAvailableSpotId = availableSpot.get().getId();
        } else {
            firstAvailableSpotId = SPOT_NOT_FOUND_FLAG;
        }
        return firstAvailableSpotId;
    }

    public List<Spot> getSpots() {
        return spots;
    }

    public Spot getSpotOnId(int spotId){
        return spots.get(spotId);
    }

    public List<VehicleToSpot> getVehicleToSpots() {
        return vehicleToSpots;
    }

    public List<Spot> getAvailableSpots(){
        List<Spot> availableSpots = spots.stream()
                .filter(spot -> spot.isAvailable())
                .collect(Collectors.toList());
        return availableSpots;
    }

    public List<VehicleToSpot> getArchivedVehicleToSpots() {
        return archivedVehicleToSpots;
    }
}
