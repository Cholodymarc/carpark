package model.database;

import model.vehicle.Vehicle;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class DatabaseVehicleDAO implements VehicleDAO {

    private List<Vehicle> vehicles = new ArrayList<>();

    public void createVehicleId(Vehicle vehicle) {
        if (vehicles.isEmpty()) {
            vehicle.setId(1);
        } else {
            vehicle.setId(vehicles.get(vehicles.size() - 1).getId() + 1);
        }
    }

    public void addVehicle(Vehicle vehicle){
        createVehicleId(vehicle);
        vehicles.add(vehicle);
    }

    public void removeVehicleOnId(int removedVehicleId){
        vehicles = vehicles.stream()
                .filter(vehicle -> vehicle.getId() != removedVehicleId)
                .collect(Collectors.toList());
    }

    public Vehicle getVehicleOnId(int vehicleId){
        Optional<Vehicle> searchedVehicle = vehicles.stream()
                .filter(vehicle -> vehicle.getId() == vehicleId)
                .findAny();
        if(searchedVehicle.isPresent()){
            return searchedVehicle.get();
        } else {
            return null;
        }
    }
}
