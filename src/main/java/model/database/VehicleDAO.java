package model.database;

import model.vehicle.Vehicle;

public interface VehicleDAO {
    Vehicle getVehicleOnId(int vehicleId);
}
